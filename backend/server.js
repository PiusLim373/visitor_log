//import express
const express = require("express");
const app = express();
const port = 5000;

//cors
const cors = require("cors");
const corsOptions = {
  origin: "*",
  credentials: true, //access-control-allow-credentials:true
  optionSuccessStatus: 200,
};

app.use(cors(corsOptions));

//import bodyparser for json handling
const bodyParser = require("body-parser");
app.use(bodyParser.json());

//connect to database
const mongoose = require("mongoose");
mongoose.connect(
  "mongodb://127.0.0.1:27017/st_engineering_assignment",
  (err) => {
    if (err) {
      console.log("database connection failed, terminating process");
      process.exit(1);
    } else console.log("connected to database");
  }
);

//http routing to /api/visitor
const visitorLoggingRouter = require("./routes/visitorLoggingRoute");
app.use("/api/visitor", visitorLoggingRouter);

//initialization
app.listen(port, () => {
  console.log(`listening at ${port}`);
});
