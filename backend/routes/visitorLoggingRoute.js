const router = require("express").Router();
const vistorModel = require("../models/visitorModel.models");

//get request for printing all data from mongodb
router.get("/", async (req, res) => {
  try {
    const postHandler = await vistorModel.find();
    res.status(200).json(postHandler);
  } catch (err) {
    res.status(400).json({ message: err });
  }
});

//post request for writing data to mongodb
router.post("/", async (req, res) => {
  // data validation, throw bad request on validation failure
  try {
    const { name, company, idNum, gender, purpose, contact } = req.body;

    if (name === "" || name === undefined) throw "Name should not be empty";
    else if (typeof name !== "string") throw "Name should be of type String";
    else if (/[^a-zA-Z ]/.test(name)) throw "Name should be alphabetical only";

    if (company === "" || company === undefined)
      throw "Company name should not be empty";
    else if (typeof company !== "string")
      throw "Company should be of type String";

    if (idNum === "" || idNum === undefined)
      throw "NRIC / Passport No should not be empty";
    else if (typeof idNum !== "string") throw "idNum should be of type String";
    else if (idNum.length > 4 || !/\d{3}\w/.test(idNum))
      throw "idNun should be 3 digits + 1 number or 4 digits";

    if (
      gender === "" ||
      gender === undefined ||
      (gender !== "Male" && gender !== "Female")
    )
      throw "gender must be Male or Female. (case sensitive)";

    if (purpose === "" || purpose === undefined)
      throw "Visit purpose should not be empty";
    else if (typeof purpose !== "string")
      throw "Purpose should be of type String";

    if (contact === "") throw "Contact person should not be empty";
    else if (typeof contact !== "string")
      throw "Contact should be of type String";
    else if (/[^a-zA-Z ]/.test(contact))
      throw "Contact person should be alphabetical only";

    console.log(Object.keys(req.body).length);
    if (Object.keys(req.body).length !== 6)
      throw "Request must contain name, company, gender, idNum, purpose and contact field";

    const visitorToSave = new vistorModel(req.body);
    const postHandler = await visitorToSave.save();
    res.json({ postHandler });
  } catch (err) {
    res.status(400).json(err);
  }
});

//experimental - get request timeout in case mongodb process died
router.get("/test_get", async (req, res) => {
  const postHandler = await vistorModel.find({}).maxTimeMS(100);
  console.log(postHandler);
  res.status(200).json(postHandler);
});

module.exports = router;
