const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Schema for data validation
const vistorScheme = new Schema(
  {
    name: { type: String, required: true },
    company: { type: String, required: true },
    idNum: { type: String, required: true },
    gender: { type: String, required: true },
    purpose: { type: String, required: true },
    contact: { type: String, required: true },
  },
  { strict: true }
);

const visitor = mongoose.model("visitor_log", vistorScheme);
module.exports = visitor;
