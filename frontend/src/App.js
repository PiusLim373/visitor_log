import './App.css';
import "bootstrap/dist/css/bootstrap.css";
import Header from './components/Header';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

import VisitorForm from './components/VisitorForm';
import VisitorView from './components/VisitorView';
import Error from './components/Error';

function App() {
  return (
    <div className="container">
      <BrowserRouter>
        <Header />
        <br />
        <Routes>
          <Route path="/" element={<VisitorForm />} />
          <Route path="/view" element={<VisitorView />} />
          <Route path="/error" element={<Error/>}/>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
