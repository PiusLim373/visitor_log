import React, { useState, useRef } from "react";
import { Row, Col, Form, Button, Spinner } from "react-bootstrap";
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckCircle } from "@fortawesome/free-solid-svg-icons";
import { useNavigate } from "react-router-dom";

export default function VisitorForm() {
  const databaseUrl = "http://localhost:5000/api/visitor";
  const navigate = useNavigate();

  const [formError, setFormError] = useState({});

  const [name, setName] = useState("");
  const [idNum, setIdNum] = useState("");
  const [company, setCompany] = useState("");
  const [gender, setGender] = useState("");
  const [purpose, setPurpose] = useState("");
  const [contact, setContact] = useState("");

  const nameRef = useRef();
  const idNumRef = useRef();
  const companyRef = useRef();
  const genderRef = useRef();
  const purposeRef = useRef();
  const contactRef = useRef();

  const [hasSubmit, setHasSubmit] = useState(false);
  const [sucessfulSubmission, setSuccessfulSubmission] = useState(false);

  // submit button function, perform data validation and focus on problematic field or send request to backend if validation pass
  const handleSubmit = (event) => {
    event.preventDefault();
    let errorInForm = {};

    //populate the form error feedback object
    if (name === "") errorInForm.name = "Name should not be empty";
    else if (/[^a-zA-Z ]/.test(name))
      errorInForm.name = "Name should be alphabetical only";

    if (company === "")
      errorInForm.company = "Company name should not be empty";

    if (idNum === "")
      errorInForm.idNum = "NRIC / Passport No should not be empty";
    else if (idNum.length > 4 || !/\d{3}\w/.test(idNum))
      errorInForm.idNum = (
        <React.Fragment>
          Input should be:
          <ul>
            <li>(NRIC) three numerical digits and one letter</li>
            <li>(Passport) four numerical digits</li>
          </ul>
        </React.Fragment>
      );

    if (gender === "") errorInForm.gender = "Please select one from the list";

    if (purpose === "")
      errorInForm.purpose = "Visit purpose should not be empty";

    if (contact === "")
      errorInForm.contact = "Contact person should not be empty";
    else if (/[^a-zA-Z ]/.test(contact))
      errorInForm.contact = "Contact person should be alphabetical only";

    var errorKeys = Object.keys(errorInForm);
    // if there's error in the form, focus on the problematic input field
    if (errorKeys.length) {
      console.log(errorInForm);
      setFormError(errorInForm);
      eval(errorKeys[0] + "Ref").current.focus();
    } else {
      // validation successful, sending request to backend server
      setHasSubmit(true);
      setFormError({});
      const data = {
        name: name,
        idNum: idNum,
        company: company,
        gender: gender,
        purpose: purpose,
        contact: contact,
      };
      console.log(data);
      axios
        .post(databaseUrl, data)
        .then(() => {
          setSuccessfulSubmission(true);
        })
        .catch(() => navigate("/error"));
    }
  };

  //reset all field for next form submission
  const resetForm = () => {
    setSuccessfulSubmission(false);
    setHasSubmit(false);
    setName("");
    setIdNum("");
    setCompany("");
    setGender("");
    setPurpose("");
    setContact("");
  };

  return (
    <React.Fragment>
      <Row>
        <h1>Visitor Logging Form</h1>
        <Col md={12}>
          {!sucessfulSubmission ? (
            <Form onSubmit={handleSubmit}>
              <Form.Group className="mb-3">
                <Form.Label>
                  Name<span style={{ color: "red" }}>*</span>
                </Form.Label>
                <Form.Control
                  ref={nameRef}
                  isInvalid={formError.name}
                  onChange={(event) => setName(event.target.value.trim())}
                  type="text"
                  placeholder="Enter name"
                />
                <Form.Control.Feedback type="invalid">
                  {formError.name}
                </Form.Control.Feedback>
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>
                  Company<span style={{ color: "red" }}>*</span>
                </Form.Label>
                <Form.Control
                  ref={companyRef}
                  isInvalid={formError.company}
                  onChange={(event) => setCompany(event.target.value)}
                  type="text"
                  placeholder="Which company you are from?"
                />
                <Form.Control.Feedback type="invalid">
                  {formError.company}
                </Form.Control.Feedback>
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>
                  NRIC / Passport No<span style={{ color: "red" }}>*</span>
                </Form.Label>
                <Form.Control
                  ref={idNumRef}
                  isInvalid={formError.idNum}
                  onChange={(event) => setIdNum(event.target.value.trim())}
                  type="text"
                  placeholder="Last 4 digit of NRIC / Passport"
                />
                <Form.Control.Feedback type="invalid">
                  {formError.idNum}
                </Form.Control.Feedback>
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>
                  Gender<span style={{ color: "red" }}>*</span>
                </Form.Label>
                <Form.Select
                  ref={genderRef}
                  isInvalid={formError.gender}
                  onChange={(event) => setGender(event.target.value)}
                  aria-label="Default select example"
                >
                  <option value="">Please select</option>
                  <option value="Male">Male</option>
                  <option value="Female">Female</option>
                </Form.Select>
                <Form.Control.Feedback type="invalid">
                  {formError.gender}
                </Form.Control.Feedback>
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>
                  Purpose of visit<span style={{ color: "red" }}>*</span>
                </Form.Label>
                <Form.Control
                  ref={purposeRef}
                  isInvalid={formError.purpose}
                  onChange={(event) => setPurpose(event.target.value)}
                  type="text"
                  placeholder="Please specify"
                />
                <Form.Control.Feedback type="invalid">
                  {formError.purpose}
                </Form.Control.Feedback>
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>
                  Contact Person<span style={{ color: "red" }}>*</span>
                </Form.Label>
                <Form.Control
                  ref={contactRef}
                  isInvalid={formError.contact}
                  onChange={(event) => setContact(event.target.value)}
                  type="text"
                  placeholder="Who are you looking for?"
                />
                <Form.Control.Feedback type="invalid">
                  {formError.contact}
                </Form.Control.Feedback>
              </Form.Group>

              <Button variant="dark" type="submit" disabled={hasSubmit}>
                <Spinner
                  as="span"
                  animation="border"
                  size="sm"
                  role="status"
                  aria-hidden="true"
                  hidden={!hasSubmit}
                />
                <span className="visually-hidden">Loading...</span>
                {"  "}Submit
              </Button>
            </Form>
          ) : (
            <React.Fragment>
              <h2 className="mt-5">
                <FontAwesomeIcon icon={faCheckCircle} />
                {"  "}Visitor form submitted successfully.
              </h2>
              <Button
                variant="dark"
                className="mt-2"
                onClick={() => {
                  resetForm();
                }}
              >
                Submit another
              </Button>
            </React.Fragment>
          )}
        </Col>
      </Row>
    </React.Fragment>
  );
}
