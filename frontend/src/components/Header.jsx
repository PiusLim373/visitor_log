import React from "react";
import { Navbar, Container, Nav } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Header() {
  return (
    <Navbar bg="dark" variant="dark">
      <Container>
        <Navbar.Brand as={Link} to={"/"}>
          Interview Assignment
        </Navbar.Brand>
        <Nav className="justify-content-end">
          <Nav.Link as={Link} to={"/"} className="navbar-item">
            Form
          </Nav.Link>
          <Nav.Link as={Link} to={"/view"} className="navbar-item">
            View
          </Nav.Link>
        </Nav>
      </Container>
    </Navbar>
  );
}
