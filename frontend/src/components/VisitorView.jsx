import React, { useEffect, useState } from "react";
import { Row, Col, Table, Fade } from "react-bootstrap";
import axios from "axios";
import { useNavigate } from "react-router-dom";

export default function VisitorView() {
  const databaseUrl = "http://localhost:5000/api/visitor";
  const navigate = useNavigate();
  const [visitorList, setVisitorList] = useState([]);
  const [loading, setloading] = useState(true);

  // componentDidMount lifecycle hook, getting data from backend server with get request
  useEffect(() => {
    axios
      .get(databaseUrl)
      .then((response) => {
        setVisitorList(response.data);
        setloading(false);
      })
      .catch(() => navigate("/error"));
  }, []);

  return (
    <React.Fragment>
      <Row>
        <h1>View Submitted Form</h1>
        <Col md={12}>
          <Fade in={!loading}>
            {!visitorList.length ? (
              <h2 className="mt-5">No record found.</h2>
            ) : (
              <Table striped bordered hover size="sm" responsive>
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Company</th>
                    <th>NRIC / Passport No</th>
                    <th>Gender</th>
                    <th>Purpose of Visit</th>
                    <th>Contact Person</th>
                  </tr>
                </thead>
                <tbody>
                  {visitorList.map((x, index) => (
                    <tr key={index}>
                      <td>{index + 1}</td>
                      <td>{x.name}</td>
                      <td>{x.company}</td>
                      <td>{x.idNum}</td>
                      <td>{x.gender}</td>
                      <td>{x.purpose}</td>
                      <td>{x.contact}</td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            )}
          </Fade>
        </Col>
      </Row>
    </React.Fragment>
  );
}
