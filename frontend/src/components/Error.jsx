import React from "react";
import { Row, Col, Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
export default function Error() {
  const navigate = useNavigate();
  return (
    <React.Fragment>
      <Row>
        <Col md={12}>
          <h1 className="mt-5">Oops..</h1>
          <h2>Something went wrong, please try again.</h2>

          <Button variant="dark" className="mt-2" onClick={() => navigate("/")}>
            Back to home page
          </Button>
        </Col>
      </Row>
    </React.Fragment>
  );
}
