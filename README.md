# ST Engineering Interview Assignment - Visitor Logging Form

## Introduction and Features

An assignment to test the short-listed interviewee on self-learning ability and assess his software knowledge.  
This WebUI consists of 2 parts:

- Visitor logging form
- View previously submitted form

This fullstack WebUI is built with a standard MERN stack. Utilizing MongoDB as its database, Node.js as its Backend API server, Express as middleware and lastly React.js as its frontend UI.

## Building and Installation

This WebUI requires [Node.js](https://nodejs.org/) and [MongoDB Community Edition](https://www.mongodb.com/try/download/community) to run.  
Recommended version for Node.js: v16.14.0 and above  
Recommended version for MongoDB: v5.0.3 and above  
A GUI tool for MongoDB, [MongoCompass](https://www.mongodb.com/products/compass), is highly recommended. (Windows users will have this installed automatically)  
Alternatively, a shell version, [Mongosh](https://www.mongodb.com/docs/mongodb-shell/install/#std-label-mdb-shell-install), is needed for database creation.

### Clone the respitory

```sh
git clone https://PiusLim373@bitbucket.org/PiusLim373/visitor_log.git
```

### Install dependencies for Backend server and start

A database and collection is required for this WebUI to work. Use the following details for database and collection creation:

- Database Name: st_engineering_assignment
- Collection Name: visitor_logs

##### Database creation with MongoCompass (recommended)

Follow the steps below for database and collection creation:  

![Database creation](documentations/database_creation.jpg)

A sample MongoDB database collection export is also provided: backend/visitor_logs_mongo_export.json  
Follow this [guide](https://www.mongodb.com/docs/compass/current/import-export/) to import the sample data. This import is not mandatory as the WebUI is capable to populating the database itself.

##### Database creation without MongoCompass

Use the commands below to for database and collection creation:

```sh
mongosh
use st_engineering_assignment
db.createCollection("visitor_logs")
```

The output `switched to db st_engineering_assignment` and `{ok: 1}` will be shown on the shell window after each line indicating creation is successful.

##### Backend server dependencies installation and starting

After database and collection creation, use the commands below to install backend server's dependencies and execution

```sh
cd visitor_log/backend
npm i
npm start
```

### Install dependencies for Frontend server and start

```sh
cd visitor_log/frontend
npm i
npm start
```

A browser window will open with the WebUI launch, if it is not, click [here](http://localhost:3000/)

## Usage

### Visitor Logging Form

On the [Visitor Logging Form Page](http://localhost:3000/), user will need to key in data for all fields and press the "Submit" button at the bottom.  
The data will be validated by the WebUI, if all fields' data are correct, it will be written into the server.  
If any of the fields' data didn't pass the validation stage, specific field will be highlighted and focussed and user will be prompted to amend the input data.  

![Problematic Input](documentations/problematic_input.jpg)  
![Successful Submission](documentations/successful_submission.jpg)

### View Submitted Form

On the [View Submitted Form Page](http://localhost:3000/view), user will be able to see previously submitted data printed nicely in table form.  

![View Submitted Form](documentations/view_submitted_form.jpg)
